let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public')
   .sass('resources/assets/scss/app.scss', 'public')